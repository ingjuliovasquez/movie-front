import { createApp } from "vue";
import Vuex from "vuex";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import "./assets/css/nucleo-icons.css";
import "./assets/css/nucleo-svg.css";
import "./assets/css/app.css";
import ArgonDashboard from "./argon-dashboard";
import { localize } from '@vee-validate/i18n';
import es from '@vee-validate/i18n/dist/locale/es.json'
import { configure, defineRule } from "vee-validate";
import VueNotifications from "vue-notifications";
import "jquery";
import toastr from "toastr";
import "toastr/build/toastr.min.css";
import plugin from './plugins/axios'
import vfmPlugin from 'vue-final-modal'

/* import the fontawesome core */
import { library } from "@fortawesome/fontawesome-svg-core";
/* import font awesome icon component */
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
/* import specific icons */
import { fas } from "@fortawesome/free-solid-svg-icons";

defineRule('required', value => {
  if (!value || !value.length) {
    return 'Campo requerido';
  }
  return true;
});
defineRule('email', value => {
  // Field is empty, should pass
  if (!value || !value.length) {
    return true;
  }
  // Check if email
  if (!/^\w+([.-_+]?\w+)*@\w+([.-]?\w+)*(\.\w{2,10})+$/ .test(value)) {
    return 'Debe ser un correo valido';
  }
  return true;
});

configure({
  generateMessage: localize({
    es
  })
})

const toast = ({ title, message, type, timeout }) => {
  if (type === VueNotifications.types.warn) type = "warning";
  return toastr[type](message, title, { timeOut: timeout });
};

const options = {
  success: toast,
  error: toast,
  info: toast,
  warn: toast,
};

/* add icons to the library */
library.add(fas);


const appInstance = createApp(App).component("font-awesome-icon", FontAwesomeIcon);
appInstance.use(store);
appInstance.use(router);
appInstance.use(ArgonDashboard);
appInstance.use(VueNotifications, options);
appInstance.use(Vuex);
appInstance.use(vfmPlugin)
appInstance.use(plugin);
appInstance.mount("#app");
