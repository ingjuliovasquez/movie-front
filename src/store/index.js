import { createStore } from "vuex";
import movies from './modules/movies';
import schedules from "./modules/schedules";
import authentication from "./modules/authentication"
import VuexPersistence from 'vuex-persist';

const vuexLocal = new VuexPersistence({
  // key: 'auth',
  storage: window.localStorage,
  modules: ['auth']
})

export default createStore({
  state: {
    hideConfigButton: false,
    isPinned: true,
    showConfig: false,
    sidebarType: "bg-white",
    isRTL: false,
    mcolor: "",
    darkMode: false,
    isNavFixed: false,
    isAbsolute: false,
    showNavs: true,
    showSidenav: true,
    showNavbar: true,
    showFooter: true,
    showMain: true,
    layout: "default",
    loading: false,
    token: null,
    errors: [],
  },
  mutations: {
    SET_LOADING(state, bool){
      state.loading = bool
    },
    SET_TOKEN(state, token){
      state.token = token
    },
    CLEAR_ERRORS(state){
      state.errors = []
    },
    toggleConfigurator(state) {
      state.showConfig = !state.showConfig;
    },
    navbarMinimize(state) {
      const sidenav_show = document.querySelector(".g-sidenav-show");

      if (sidenav_show.classList.contains("g-sidenav-hidden")) {
        sidenav_show.classList.remove("g-sidenav-hidden");
        sidenav_show.classList.add("g-sidenav-pinned");
        state.isPinned = true;
      } else {
        sidenav_show.classList.add("g-sidenav-hidden");
        sidenav_show.classList.remove("g-sidenav-pinned");
        state.isPinned = false;
      }
    },
    sidebarType(state, payload) {
      state.sidebarType = payload;
    },
    navbarFixed(state) {
      if (state.isNavFixed === false) {
        state.isNavFixed = true;
      } else {
        state.isNavFixed = false;
      }
    }
  },
  actions: {
    toggleSidebarColor({ commit }, payload) {
      commit("sidebarType", payload);
    }
  },
  getters: {},
  modules: {
    movies: movies,
    schedules: schedules,
    auth: authentication
  },
  plugins: [vuexLocal.plugin]
});
