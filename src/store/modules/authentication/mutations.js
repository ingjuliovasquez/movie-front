export function SET_USER(state, user) {
    state.user = user
    state.isLogged = true
    state.error = false
    state.errorMessage = ''
}

export function LOGOUT(state) {
    state.user = null
    state.isLogged = false
}

export function AUTH_ERROR(state, error) {
    state.error = true
    state.errorMessage = error
    state.user = null
    state.isLogged = false
}

export function CLEAR_ERRORS(state) {
    state.error = false
    state.errorMessage = ''
}
