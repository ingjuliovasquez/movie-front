export async function singIn(context, user) {
  context.commit("SET_LOADING", true, { root: true });
  await this.$axios
    .post('/login', user)
    .then((response) => {
      console.log(response.data);
      context.commit("SET_USER", response.data.user);
      return Promise.resolve("success");
    })
    .catch((error) => {
      console.warn(error.response.data.errors[0]);
      context.commit("AUTH_ERROR", error.response.data.errors[0]);
      return Promise.reject("error");
    })
    .finally(() => {
      context.commit("SET_LOADING", false, { root: true });
    });
}

export const login = async ({ commit }, data) => {
  commit("SET_LOADING", true, { root: true });
  await this.$axios
    .post("http://127.0.0.1:8000/api/login", data)
    .then((response) => {
      if (response.status === 200) {
        console.log(response.data.token, response.data.user)
        commit("SET_TOKEN", response.data.token, { root: true });
        commit("SET_USER", response.data.user);
      }
    })
    .catch((error) => {
      return Promise.reject(error.response.data.message);
    })
    .finally(() => {
      commit("SET_LOADING", false, { root: true });
    });
};
