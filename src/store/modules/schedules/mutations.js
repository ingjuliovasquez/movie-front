export const SET_SCHEDULE_MOVIE = (state, payload) => {
    state.movie_schedules = payload
}

export const SET_SCHEDULES = (state, payload) => {
    state.scheduleList = payload
}

export const SET_SCHEDULE = (state, payload) => {
    state.scheduleList.push(payload)
}

export const UPDATE_SCHEDULE = (state, payload) => {
    const result = state.scheduleList.findIndex(schedule => schedule.id == payload.id)
    state.scheduleList.splice(result, 1, payload)
}

export function DELETE_SCHEDULE(state, schedule_id) {
    const result = state.scheduleList.findIndex(schedule => schedule.id == schedule_id)
    state.scheduleList.splice(result, 1)
}