import axios from "axios";

const URL = "http://127.0.0.1:8000/api/schedules";

export const fetchSchedules = async ({ commit }) => {
  commit("SET_LOADING", true, { root: true });
  await axios
    .get(URL, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
    .then((response) => {
      commit("SET_SCHEDULES", response.data);
    })
    .catch((error) => {
      return Promise.reject(error);
    })
    .finally(() => {
      commit("SET_LOADING", false, { root: true });
    });
};

export const createSchedule = async ({ commit }, schedule) => {
  commit("SET_LOADING", true, { root: true });
  await axios
    .post(URL, schedule, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
    .then((response) => {
      commit("SET_SCHEDULE", response.data);
      return Promise.resolve("success");
    })
    .catch((error) => {
      return Promise.reject(error.response);
    })
    .finally(() => {
      commit("SET_LOADING", false, { root: true });
    });
};

export const assignSchedule = async ({ commit }, data) => {
  commit("SET_LOADING", true, { root: true });
  await axios
    .post("http://127.0.0.1:8000/api/assing-schedules", data, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
    .then((response) => {
      commit("SET_SCHEDULE_MOVIE", response.data);
      return response
    }).catch(error => {
      return error
    });
};
