export const SET_MOVIES = (state, payload) => {
    state.movieList = payload
}

export const SET_MOVIE = (state, payload) => {
    state.movieList.push(payload)
}

export const UPDATE_MOVIE = (state, payload) => {
    const result = state.movieList.findIndex(movie => movie.id == payload.id)
    state.movieList.splice(result, 1, payload)
}

export function DELETE_MOVIE(state, movie_id) {
    const result = state.movieList.findIndex(movie => movie.id == movie_id)
    state.movieList.splice(result, 1)
}