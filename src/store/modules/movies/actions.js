import axios from "axios";

const url = "http://127.0.0.1:8000/api/movies";

export const fetchMovies = async ({ commit }) => {
  commit("SET_LOADING", true, { root: true });
  await axios
    .get(url, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
    .then((response) => {
      commit("SET_MOVIES", response.data);
    })
    .catch((error) => {
      return Promise.reject(error);
    })
    .finally(() => {
      commit("SET_LOADING", false, { root: true });
    });
};

export const createMovie = async ({ commit }, movie) => {
  commit("SET_LOADING", true, { root: true });
  await axios
    .post(url, movie, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
    .then((response) => {
      commit("SET_MOVIE", response.data);
      return Promise.resolve("success");
    })
    .catch((error) => {
      return Promise.reject(error.response);
    })
    .finally(() => {
      commit("SET_LOADING", false, { root: true });
    });
};

