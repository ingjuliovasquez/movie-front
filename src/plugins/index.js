import Vue from 'vue'
import "./axios"
require('./vee-validate')

/**
 *  Vue-SweetAlert2
 */
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css'
Vue.use(VueSweetalert2)

/**
*  V-Filters
*/
// import Vue2Filters from 'vue2-filters'
// Vue.use(Vue2Filters)

/**
 *  Moment Js
 */
window.moment = require('moment');


/**
 *  Dropzone
 */
import vue2Dropzone from 'vue2-dropzone'
import 'vue2-dropzone/dist/vue2Dropzone.min.css'
Vue.component('vueDropzone', vue2Dropzone)

/**
 *  V-Modal
 */
import VueJSModal from 'vue-js-modal';
Vue.use(VueJSModal)

/**
 *  Vuetable 2
 */
import Vuetable from 'vuetable-2'
Vue.component('vuetable', Vuetable)

/**
 *  Vue Paginate
 */
import VuePaginate from 'vue-paginate'
Vue.use(VuePaginate)
