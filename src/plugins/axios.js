import axios from 'axios'
import { inject } from 'vue'

const injectionKey = Symbol('http')

export const useHttp = () => inject(injectionKey)

export const plugin = {
  install (app) {
    const http = axios.create(
        {
            withCredentials: true,
            baseURL: 'http://127.0.0.1:8000/api',
            headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}
          }
    )
    app.config.globalProperties.$http = http
    app.provide(injectionKey, http)
  }
}