class RenderIconsColumn {
  init() {
    this.eGui = document.createElement("span");
    const icon1 = `<font-awesome-icon icon="fa-solid fa-clapperboard" class="text-primary text-sm opacity-10" />`
    this.eGui.innerHTML = `
     ${icon1}
    `;
  }

  getGui() {
    return this.eGui;
  }

  refresh() {
    return false;
  }
}

export default RenderIconsColumn;
