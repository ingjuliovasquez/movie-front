import { createRouter, createWebHistory } from "vue-router";
import Dashboard from "../views/Dashboard.vue";
import Profile from "../views/Profile.vue";
import Tables from "../views/Tables.vue";
import Signin from "../views/auth/Signin.vue";
import MoviesForm from "../views/movies/MoviesForm.vue"
import Movies from "../views/movies/Movies.vue"
import MovieAssingSchedule from '../views/movies/MovieAssignSchedule.vue'
import Schedule from '../views/schedules/Schedule.vue'
import ScheduleForm from '../views/schedules/ScheduleForm.vue'
import store from "../store";

const routes = [
  {
    path: '/',
    meta: {auth: true},
    component: Dashboard,
  },
  {
    path: "/inicio",
    name: 'inicio',
    meta: {auth: true},
    component: Dashboard
  },
  {
    path: "/profile",
    name: "Profile",
    meta: {auth: true},
    component: Profile,
  },
  {
    path: "/movies",
    name: "movies",
    meta: {auth: true},
    component: Movies,
  },
  {
    path: "/movies-form",
    name: "movies-form",
    meta: {auth: true},
    component: MoviesForm,
  },
  {
    path: "/schedules",
    name: "schedules",
    meta: {auth: true},
    component: Schedule,
  },
  {
    path: "/schedule-form",
    name: "schedule-form",
    meta: {auth: true},
    component: ScheduleForm,
  },
  {
    path: "/tables",
    name: "tables",
    meta: {auth: true},
    component: Tables,
  },
  {
    path: "/assign-schedule/:id",
    name: "assign-schedule",
    meta: {auth: true},
    component: MovieAssingSchedule,
  },
  {
    path: '/signin',
    name: 'auth',
    meta: {auth:false},
    component: Signin
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  linkActiveClass: "active",
});


router.beforeEach((to, from, next) => {
  if (store.state.errors.length >= 1) {
      store.commit('CLEAR_ERRORS');
  }
  const needsAuth = to.matched.some(record => record.meta.auth )
  const isLogged = store.state.auth.isLogged

  if (!needsAuth && isLogged && to.path === '/signin') {
      next('/')
  } 
  if (needsAuth && !isLogged) {
      next('/signin')
  } else {
      next()
  }
})

export default router;
